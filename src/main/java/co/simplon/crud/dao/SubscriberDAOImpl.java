package co.simplon.crud.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import co.simplon.crud.model.Subscriber;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SubscriberDAOImpl implements SubscriberDAO {

    // INJECT DATASOURCE
    @Autowired
    private DataSource dataSource;

    // REQUETES SQL
    private static final String GET_SUBSCRIBERS = "SELECT * FROM subscribers";
    
    /**
     * retourne la liste des abonnés
     * @return
     */
    @Override
    public List<Subscriber> findAll() {
        Connection cnx;
        List<Subscriber> list = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(GET_SUBSCRIBERS);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Subscriber(
                    rs.getInt("subscriberId"),
                    rs.getString("firstName"),
                    rs.getString("lastName"),
                    rs.getString("email"),
                    rs.getTimestamp("createdAt")
                ));
            }
            cnx.close();
        } catch (Exception e) {
            e.getMessage();
        }
        return list;
    }
    
}
