package co.simplon.crud.dao;

import java.util.List;

import co.simplon.crud.model.Subscriber;

public interface SubscriberDAO {
    
    public List<Subscriber> findAll();
}
