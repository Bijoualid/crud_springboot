package co.simplon.crud.controller;

import co.simplon.crud.dao.SubscriberDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SubscriberController {
    
    // INJECT SubscriberDAO
    @Autowired
    private SubscriberDAO subscriberDAO;

    // renvoie la page index.html
    // renvoie la liste des abonnés
    @GetMapping("/")
    public String showIndexPageAndGetSubscribersList(Model model) {
        model.addAttribute("subscribers", subscriberDAO.findAll());
        return "index";
    }
    
}
